# Bitcoin (BCH) vending machine

*Bitcoin Vending Machine is a simple software that will turn your micro-computer into a fully functional vending machine.*

![rpi](http://image.noelshack.com/fichiers/2018/24/2/1528771436-rpi.jpeg)

## Software

- Web interface
- Siema JavaScript Slider

## Hardware

- Raspberry Pi 3
- LCD 3.5inch RPi display
    - 480x320 pixels

![hardware](http://image.noelshack.com/fichiers/2018/24/2/1528771434-hardware.jpeg)